# VM project installation

1. clone the repo
    ```
    git clone git@bitbucket.org:skrishtofenko/vm.git
    ```

2. install composer packages
    ```
    composer install
    ```

3. create an empty database

4. run ```php init``` to deploy needed environment

5. run ```php yii migrate``` to install main project db
