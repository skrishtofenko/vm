<?php
namespace restapi\modules\v1\controllers;

use yii;
use restapi\controllers\AppController;
use restapi\models\Orders;

class OrdersController extends AppController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter']['cors']['Access-Control-Request-Method'] = ['GET', 'POST', 'PATCH', 'HEAD', 'OPTIONS'];
        $behaviors['input-params']['rules'] = [
            'index' => [
                [['customer_id', 'description', 'total_cost'], 'required', 'on' => 'post'],
                [['id'], 'required', 'on' => 'patch'],
                [['id', 'customer_id'], 'integer'],
//                [['id', 'customer_id'], 'integer', 'on' => ['post', 'patch']],
//                [['id', 'customer_id'], 'each', 'rule' => ['integer'], 'on' => 'get'],
                [['description'], 'safe'],
                [['total_cost'], 'number'],
            ],
        ];
        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'index' => ['get', 'post', 'patch', 'put'],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->request->isPost) {
            return Orders::create($this->input);
        } elseif (Yii::$app->request->isPatch) {
            return Orders::patch($this->input);
        } else {
            return Orders::getList($this->input);
        }
    }
}