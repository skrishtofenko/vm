<?php
namespace restapi\controllers;

use Yii;
use yii\rest\Controller;
use yii\filters\Cors as CorsFilter;
use restapi\filters\RequestInputFilter;

/**
 * App controller
 */
class AppController extends Controller
{
    public $input;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['input-params'] = [
            'class' => RequestInputFilter::className(),
            'rules' => [],
        ];
        $behaviors['corsFilter'] = [
            'class' => CorsFilter::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 3600,
                'Access-Control-Request-Method' => [],
            ],
        ];
        return $behaviors;
    }

    public function init()
    {
        $this->input = Yii::$app->request->isGet ? Yii::$app->request->get() : Yii::$app->request->bodyParams;
        parent::init();
    }
}
