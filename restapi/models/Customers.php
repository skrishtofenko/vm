<?php
namespace restapi\models;

class Customers extends \common\models\Customers
{
    public $ordersIds;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['ordersIds'], 'safe'];
        return $rules;
    }

    public static function findWithOrdersIds($id = null)
    {
        $query = self::find()
            ->select(['customers.*', 'GROUP_CONCAT(`orders`.`id`) as `ordersIds`'])
            ->leftJoin('orders', 'customers.id = orders.customer_id')
            ->groupBy('customers.id');
        if ($id) {
            $query->where(['customers.id' => $id]);
        }
        $customers = $query->all();
        /** @var $customers self[] */
        if ($id && $customers[0]['id'] == null) {
            return null;
        }
        return self::map($customers);
    }

    private function mapCustomer()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'ordersIds' => !empty($this->ordersIds) ? explode(',', $this->ordersIds) : [],
        ];
    }

    private static function map($customers)
    {
        $result = [];
        foreach ($customers as $customer) {
            /** @var $customer self */
            $result[$customer->id] = $customer->mapCustomer();
        }
        return $result;
    }
}
