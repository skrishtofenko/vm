<?php
namespace restapi\models;

use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class Orders extends \common\models\Orders
{
    public static function create($order)
    {
        if ($customer = Customers::findOne($order['customer_id'])) {
            $newOrder = new self($order);
            if ($newOrder->save()) {
                return $newOrder->id;
            }
            throw new HttpException(400, 'Order creation failed');
        }
        throw new HttpException(404, 'Customer not found');
    }

    public function patch($patchOrder)
    {
        if ($order = self::findOne($patchOrder['id'])) {
            $updatableFields = ['description', 'total_cost'];
            foreach ($updatableFields as $field) {
                $order->$field = $patchOrder[$field] ?? $order->$field;
            }
            if ($order->save()) {
                return 'ok';
            }
            throw new HttpException(400, 'Order update failed');
        }
        throw new HttpException(404, 'Customer not found');
    }

    public function getList($searchParams)
    {
        $customerId = $searchParams['customer_id'] ?? null;
        $orderId = $searchParams['id'] ?? null;

        if ($customerId) {
            if ($customers = Customers::findWithOrdersIds($customerId)) {
                if ($orderId) {
                    if (in_array($orderId, $customers[$customerId]['ordersIds'])) {
                        $orders = [$orderId => self::findOne($orderId)];
                    } else {
                        throw new HttpException(404, 'Pair customer-order not found');
                    }
                } else {
                    $orders = ArrayHelper::index(
                        self::find()->where(['customer_id' => $customerId])->all(), 'id'
                    );
                }
            } else {
                throw new HttpException(404, 'Customer not found');
            }
        } else {
            if ($orderId) {
                if ($orders = self::findOne($orderId)) {
                    $orders = [$orderId => $orders];
                    $customers = Customers::findWithOrdersIds($orders[$orderId]['customer_id']);
                } else {
                    throw new HttpException(404, 'Order not found');
                }
            } else {
                $orders = ArrayHelper::index(
                    self::find()->all(), 'id'
                );
                $customers = Customers::findWithOrdersIds();
            }
        }

        return [
            'customers' => $customers,
            'orders' => $orders,
        ];
    }
}
