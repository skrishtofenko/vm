<?php
use yii\db\Migration;

class m190117_041644_fill_customers extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%customers}}', ['name' => 'Ипполит Карлович']);
        $this->insert('{{%customers}}', ['name' => 'Иванов Ваня']);
        $this->insert('{{%customers}}', ['name' => 'Федоров Илья']);
        $this->insert('{{%customers}}', ['name' => 'Оксюморон Апполинарьевич']);
        $this->insert('{{%customers}}', ['name' => 'Фидель Кастро']);
        $this->insert('{{%customers}}', ['name' => 'Рюрик Околокаминосидящий']);
        $this->insert('{{%customers}}', ['name' => 'Иван Грозный']);
        $this->insert('{{%customers}}', ['name' => 'Андрей Добрый']);
    }

    public function safeDown()
    {
        $this->truncateTable('{{%customers}}');
    }
}
