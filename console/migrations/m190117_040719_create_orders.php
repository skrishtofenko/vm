<?php
use yii\db\Migration;

class m190117_040719_create_orders extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'description' => $this->text()->notNull(),
            'total_cost' => $this->decimal(10, 2)->notNull(),
        ], $tableOptions);

        $this->addForeignKey('FK_orders_customer_id', '{{%orders}}', 'customer_id', '{{%customers}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_orders_customer_id', '{{%orders}}');
        $this->dropTable('{{%orders}}');
    }
}
